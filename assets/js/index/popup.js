$('.js-callback').click(function () {
    $('.overlay').fadeIn(500);
    $('.callback').fadeIn(500);
});

$('.js-callbackClose').click(function () {
    $('.callbackForms').show(500);
    $('.callbackResult').hide(500);
    $('.overlay').hide();
    $('.callback').hide();

});

$('.js-callbackResult').click(function () {
    $('.callbackForms').hide();
    $('.callbackResult').fadeIn(100);
});

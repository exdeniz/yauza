var express = require("express");

var app = express();
    app.set('views', __dirname + '/assets/template')
    app.set('view engine', 'jade')
    app.use(express.static(__dirname + '/public'))
    app.listen(3030)
    app.get('/', function (req, res) {
          res.render('index')
        });
    app.get('/:file', function (req, res) {
          res.render(req.params.file)
        });
    console.log('Listening on port: 3001');

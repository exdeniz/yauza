

$('.js-callback').click(function () {
    $('.overlay').fadeIn(500);
    $('.callback').fadeIn(500);
});

$('.js-callbackClose').click(function () {
    $('.callbackForms').show(500);
    $('.callbackResult').hide(500);
    $('.overlay').hide();
    $('.callback').hide();

});

$('.js-callbackResult').click(function () {
    $('.callbackForms').hide();
    $('.callbackResult').fadeIn(100);
});

$('.sliderItems').slick();

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
$('.js-email').click(function () {
    if (IsEmail($("#email").val())) {
        $(".bannerBlockFormInput").fadeOut('fast');
        $(".bannerBlockFormComplete").addClass('bannerBlockFormCompleteShow');
        sendServer($("#email").val());
    }
    else
        $('#email').addClass("password-error");
})

$('#email').keypress(function() {
    $('#email').removeClass("password-error")
});

function sendServer(val) {
    var data = 'js=feedback_menu&email=' + val + '&subject=market&message=test'
    $.ajax({
      type: "POST",
      url: 'http://wada.vn/feedback.html',
      data: data,
    });

}

$('input,textarea').focus(function(){
   $(this).data('placeholder',$(this).attr('placeholder'))
   $(this).attr('placeholder','');
});
$('input,textarea').blur(function(){
   $(this).attr('placeholder',$(this).data('placeholder'));
});

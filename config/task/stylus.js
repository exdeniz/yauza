var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    pixrem = require('gulp-pixrem'),
    prefix = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    postcss = require('gulp-postcss'),
    assets  = require('postcss-assets'),
    mergeRules = require('postcss-merge-rules'),
    reload = browserSync.reload;

// Собираем Stylus
gulp.task('stylus2', function() {
    var processors = [
        mergeRules()
    ]
    gulp.src('./assets/b/blocks.styl')
        .pipe(stylus()) // собираем stylus
        // .pipe(cssBase64({
        //     baseDir: "../../assets/icons",
        // }))
        .pipe(postcss([assets({
              loadPaths: ['assets/icons/']
          })]))
        //.pipe(postcss(processors))
        .pipe(prefix({
            browsers: ['last 2 versions', 'IE 9', 'IE 10']
        }))
        .pipe(gulp.dest('./public/css/')) // записываем css
        .pipe(reload({
            stream: true
        },
            "./public/css/blocks.css"
            ));
});


gulp.task("stylus", function() {
    var processors = [
        assets({
              loadPaths: ['assets/icons/']
          })
    ];
    gulp.src("./assets/b/blocks.styl")
        .pipe(stylus())
        .pipe(postcss(processors))
        .pipe(prefix({
             browsers: ['last 2 versions', 'IE 9', 'IE 10']
        }))
        .pipe(gulp.dest('./public/css/'))
        .pipe(reload({stream: true}));

});
